import random

while(True):
    cpuAction = random.randint(0, 2)
    action = int(input("Type in a number representing your action... 0=rock, 1=paper, and 2=scissors..."))
    print("The CPU picked:" ,cpuAction)
    if(action == 0 and cpuAction == 1):
        print("Paper beats rock; you lose!!!!!!!!!!!!")
        break
    if(action == 1 and cpuAction == 0):
        print("Paper beats rock; you win!!!!!!!!!!!!!")
        break
    if(action == 2 and cpuAction == 0):
        print("Rock beats scissors; you lose!!!!!!!!!")
        break
    if(action == 0 and cpuAction == 2):
        print("Rock beats scissors; you lose!!!!!!!!!")
        break
    if(action == 1 and cpuAction == 2):
        print("Scissors beats paper; you lose!!!!!!!!!!")
        break
    if(action == 2 and cpuAction == 1):
        print("Scissors beats paper; you win!!!!!!!!!!")
        break
    if (action == cpuAction):
        print("Tie, go again")